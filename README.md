# johnorford.net



## Getting started

Run:

```
nix develop
elm make src/Main.elm --output public/elm-app.js
# Elm Reactor(?)
elm reactor(?)
# Open editor
open -a Emacs
code .
```

## Deployment

```
firebase deploy
```


