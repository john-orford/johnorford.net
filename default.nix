{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.elmPackages.elm pkgs.elmPackages.elm-format
    pkgs.elmPackages.elm-language-server pkgs.nodePackages.firebase-tools
    pkgs.nodejs
  ];

  shellHook = ''
      npm install elm-live
      export PS1="\W $ "
      echo "An Elm Nix shell."
      '';
}
