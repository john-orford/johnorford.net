module Content exposing (Postcard, ps)


type alias Postcard =
    { description : String
    , pathToPicture : String
    }


ps : List Postcard
ps =
    [ Postcard "Haskell. Precise software." "haskell.png"
    , Postcard "Elm. Lego-like web dev." "elm.jpg"
    , Postcard "Nix. Declarative environment and PC configuration." "nix.png"
    , Postcard "Linux. A buffet of delicious operating systems." "linux.png"
    , Postcard "Cloud. All the hardware, none of the cables." "cloud.png"
    , Postcard "Indonesia. Surprises and smiles." "indonesia.webp"
    , Postcard "Marvel Snap. Presence of mind counts most." "marvel snap.webp"
    , Postcard "Kingdom Rush. TIL: narrow optimisation rarely beats hordes of enemies." "kingdom rush.webp"
    , Postcard "Pure Functions. Where = really means =" "pure functions.webp"
    , Postcard "Scarlet Nexus. Satisfying battling." "scarlet nexus.webp"
    , Postcard "North Eastern China Rice. How have I become a rice connoisseur?" "rice.webp"
    , Postcard "Expensive Tequila." "tequila.webp"
    , Postcard "Jever Beer. Crisp and northern." "jever.webp"
    , Postcard "Haskell Language Server. Makes otherwise plodding coding flow." "hls.png"
    , Postcard "Emacs. Crunch through problems with the power of typing on a keyboard." "emacs.webp"
    , Postcard "Aeropress. Simple coffee." "aeropress.webp"

    -- , Postcard "Cheap Irish Whiskey: Good value for money, but may lack the complexity and depth of more aged or premium whiskies." ""
    -- , Postcard "Middle Range Scotch Whiskey (JW Black): Balanced and accessible, but might not satisfy connoisseurs seeking the nuance of single malts or higher-end blends." ""
    -- , Postcard "IPA: Popular in the craft beer scene, but its bitterness and hop intensity can be off-putting for some." ""
    -- , Postcard "Programming on an iPad Pro 13 inch: Innovative and portable, but the touch interface and iOS limitations might hinder more complex programming tasks." ""
    -- , Postcard "Chive Dumplings: A delicious dish but can be high in carbs and not suitable for those on certain diets." ""
    , Postcard "Chinese Dill Pancakes. From the North East. The heavy metal of Chinese cuisine." "dill_pancake.webp"

    -- , Postcard "Machine Learning." ""
    -- , Postcard "Statistics" ""
    -- , Postcard "Front-End Programming." ""
    -- , Postcard "Type-Level Programming." ""
    -- , Postcard "Dependent Types." ""
    -- , Postcard "Program Correctness & Types." ""
    -- , Postcard "Football." ""
    , Postcard "Etymology. Clock & Glocke come from Irish clog. Apparently. " "etymology.webp"

    -- , Postcard "Programming Abstractions." ""
    -- , Postcard "GoldenEye: A revolutionary game in its time, but modern gamers might find its graphics and controls outdated." ""
    -- , Postcard "Mario Kart: Provides great fun and competition, but its reliance on luck-based items can frustrate players seeking a purely skill-based racing game." ""
    -- , Postcard "Mario: Iconic and foundational in gaming history, but some may find the formula repetitive across the series." ""
    -- , Postcard "Street Fighter 2: A classic in the fighting genre, but newer players might find it less accessible compared to modern fighting games." ""
    , Postcard "Nier Automata. Confusion in a hail of bullets. Plus quirky side games." "nier.png"
    , Postcard "Cheap Fragrant Baijiu." "baijiu.webp"
    , Postcard "Tmux. Persistant remote shells etc." "tmux.png"
    , Postcard "The Boys & Gen V. Bloody dark humour." "theboys.jpg"
    , Postcard "Mr and Mrs Smith. Pleasantly dark like dark chocolate." "mr_and_mrs_smith.png"
    , Postcard "Prancing Pony Podcast." "prancing_pony_podcast.jpg"
    , Postcard "Cyberpunk: Edgerunners." "cyberpunk.jpg"
    , Postcard "Castlevania." "Castlevania.webp"
    , Postcard "Mosh. Shells for ropey internet connections." "mosh.png"
    , Postcard "Marmite." "marmite.png"
    , Postcard "Broadcasting House. Sunday morning on the BBC." "BROADCASTING.png"
    , Postcard "Shogun. Cultural misunderstandings and mistranslations" "shogun.png"
    , Postcard "Sherlock Holmes. Out of copyright and infinitely more fruitful than almost every other fictional character." "sherlock.png"
    ]
