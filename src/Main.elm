module Main exposing (main)

import Browser
import Browser.Events as Events
import Content
import Element exposing (..)
import Element.Background as Background
import Element.Events exposing (..)
import Element.Font as Font
import Html exposing (Html)
import Html.Attributes exposing (id)
import List


type alias Model =
    Device


type Msg
    = SetScreenSize Int Int


type alias Flags =
    { height : Int
    , width : Int
    }


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


init : Flags -> ( Model, Cmd Msg )
init flags =
    ( classifyDevice
        { height = flags.height
        , width = flags.width
        }
    , Cmd.none
    )



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg _ =
    case msg of
        SetScreenSize x y ->
            let
                classifiedDevice =
                    classifyDevice
                        { width = x
                        , height = y
                        }
            in
            ( classifiedDevice, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch [ Events.onResize (\values -> SetScreenSize values) ]



-- main : Html msg
-- main =
--     view
--view : Html msg


h spacerLength navItemSpacing navItemPadding navItemFontSize backgroundRowPaddingX backgroundRowPaddingY =
    let
        spacer : Element msg
        spacer =
            el [ Element.width (px spacerLength) ] (text "")

        navMenu : Element msg
        navMenu =
            let
                navItem : String -> Element msg
                navItem textValue =
                    el [ padding navItemPadding, mouseOver [ Background.color <| rgb255 240 240 240 ] ] (link [] { label = text textValue, url = "#" ++ textValue })
            in
            row [ spaceEvenly, Font.size navItemFontSize, Font.color (rgb255 80 80 80), Font.family [ Font.sansSerif ], centerY ]
                [ navItem "Home"
                , navItem "About"
                , navItem "Services"
                , navItem "Contact"
                ]
    in
    row [ Background.color <| rgb255 230 240 250, paddingXY backgroundRowPaddingX backgroundRowPaddingY, width fill, centerY ]
        [ Element.el [ htmlAttribute (id "Home"), centerX, width shrink, Font.color <| rgb255 80 80 80, Font.size 30, Font.heavy, Font.family [ Font.sansSerif ] ] (text "John Orford")
        , spacer
        , navMenu
        ]


view : Model -> Html Msg
view model =
    let
        header : Element msg
        header =
            case ( model.class, model.orientation ) of
                ( Phone, Portrait ) ->
                    h 0 0 2 12 1 10

                _ ->
                    h 20 30 10 20 40 40

        mainBody : Element msg
        mainBody =
            let
                heroImageArea : Element msg
                heroImageArea =
                    el [ Background.image "/assets/bkgd.webp", width fill, height (px 500) ] (text "")

                contentList : Element msg
                contentList =
                    let
                        content : { a | description : String, pathToPicture : String } -> Element msg
                        content t =
                            wrappedRow [ width fill, padding 50, htmlAttribute (id "About") ]
                                [ column
                                    [ alignTop
                                    , padding 5
                                    , Font.size 30
                                    , width fill
                                    ]
                                    [ paragraph [] [ text t.description ] ]
                                , column
                                    [ width fill
                                    , padding 5
                                    ]
                                    [ image [ width fill ] { src = "/assets/" ++ t.pathToPicture, description = "" }
                                    ]
                                ]

                        contentPhone : { a | description : String, pathToPicture : String } -> Element msg
                        contentPhone t =
                            column [ width fill, padding 50, htmlAttribute (id "About") ]
                                [ row
                                    [ alignTop
                                    , padding 5
                                    , Font.size 30
                                    , width fill
                                    ]
                                    [ paragraph [] [ text t.description ] ]
                                , row
                                    [ width fill
                                    , padding 5
                                    ]
                                    [ image [ width fill ] { src = "/assets/" ++ t.pathToPicture, description = "" }
                                    ]
                                ]
                    in
                    case ( model.class, model.orientation ) of
                        ( Phone, Portrait ) ->
                            column [ width fill, spacing 50, Font.color (rgb255 80 80 80) ]
                                ([ wrappedRow [ width fill, padding 50, htmlAttribute (id "About") ]
                                    [ column
                                        [ alignTop
                                        , padding 5
                                        , Font.size 30
                                        , width fill
                                        ]
                                        [ paragraph [] [ text "About. Me." ] ]
                                    ]
                                 ]
                                    ++ List.map contentPhone Content.ps
                                    ++ [ wrappedRow [ width fill, padding 50, htmlAttribute (id "Services") ]
                                            [ column
                                                [ alignTop
                                                , padding 5
                                                , Font.size 30
                                                , width fill
                                                , height (px 300)
                                                ]
                                                [ paragraph [] [ text "Services. Code." ] ]
                                            ]
                                       ]
                                    ++ [ wrappedRow [ width fill, padding 50, htmlAttribute (id "Contact") ]
                                            [ column
                                                [ alignTop
                                                , padding 5
                                                , Font.size 30
                                                , width fill
                                                , height (px 300)
                                                ]
                                                [ paragraph [] [ text "Contact. John.Orford@gmail.com." ] ]
                                            ]
                                       ]
                                )

                        _ ->
                            column [ width fill, spacing 50, Font.color (rgb255 80 80 80) ]
                                ([ wrappedRow [ width fill, padding 50, htmlAttribute (id "About") ]
                                    [ column
                                        [ alignTop
                                        , padding 5
                                        , Font.size 30
                                        , width fill
                                        ]
                                        [ paragraph [] [ text "About. Me." ] ]
                                    ]
                                 ]
                                    ++ List.map content Content.ps
                                    ++ [ wrappedRow [ width fill, padding 50, htmlAttribute (id "Services") ]
                                            [ column
                                                [ alignTop
                                                , padding 5
                                                , Font.size 30
                                                , width fill
                                                , height (px 300)
                                                ]
                                                [ paragraph [] [ text "Services. Code." ] ]
                                            ]
                                       ]
                                    ++ [ wrappedRow [ width fill, padding 50, htmlAttribute (id "Contact") ]
                                            [ column
                                                [ alignTop
                                                , padding 5
                                                , Font.size 30
                                                , width fill
                                                , height (px 300)
                                                ]
                                                [ paragraph [] [ text "Contact. John.Orford@gmail.com." ] ]
                                            ]
                                       ]
                                )
            in
            column [ padding 0, width fill ]
                [ heroImageArea
                , contentList
                ]

        footer : Element msg
        footer =
            row [ Background.color <| rgb255 230 240 250, padding 20, width fill, centerY ]
                [ Element.el [ centerX, Font.color <| rgb255 80 80 80, Font.family [ Font.sansSerif ], width fill ] (text "Can't see the forest from the trees") ]
    in
    layout [ height fill, width fill ]
        (column [ height fill, width fill, spacing 20 ]
            [ header
            , mainBody
            , footer
            ]
        )
